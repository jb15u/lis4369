
# LIS4369 - Extensible Enterprise Solutions

## Jarod Barclay

### Assignment 3 Requirements:

*Three Parts:*

1. Backward-engineer (using .NET Core) console application screenshots
2. Display assignment requirements and my name as the author
3. Display current date/time
4. Perform and display future value calculation, data validation, decimal data type, and currency formatting.

#### README.md file should include the following items:

* Screenshot of Future Value Calculator

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>

#### Assignment Screenshots:

*Screenshot of future value calculator*:

![Valid Operation Screenshot](img/ss1.png)

