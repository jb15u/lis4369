using System;

namespace ConsoleApplication
{
    public class Program
    {
	public static void Main(string[] args)
	{
	string requirements =
	@"///////////////////////
	Program Requirements:
	A3 - Future Value Calculator
	Author: Jarod Barclay
	1) Use intrinsic method to display date/time;
	2) Research: What is future vale? And, its formula
	3) Create FutureValue method using the following parameters: decimal presentValue, int numYears, decimal yearlyInt, decimal monthlyDep;
	4) Initialize suitable variables: use decimal data types for currency variables;
	5) Perform data validation: prompt user until correct data is entered;
	6) Display money in currency format;
	7) Allow user to press any key to return back to command line.
	///////////////////////";
	Console.WriteLine(requirements);

	Console.WriteLine("\nNow: " + DateTime.Now.ToString("ddd, M/d/yy h:mm:ss t"));
 	Console.WriteLine();

    //Declare variables
    int numYears = 0;
    decimal presentValue, yearlyInt, monthlyDep, future = 0.0M;



    // Step #1:  Prompt the user to enter the initial balance
     Console.WriteLine("Starting Balance: ");
     while (!decimal.TryParse(Console.ReadLine(), out presentValue))
   	{
   	    Console.WriteLine("Starting balance must be numeric.");
   	}


    // Step #2:  Prompt the user to enter the number of years
    Console.WriteLine("Term (years): ");
    while (!int.TryParse(Console.ReadLine(), out numYears))
   {
       Console.WriteLine("Term must be integer data type.");
   }


    // Step #3:  Prompt the user to enter interest rate
    Console.WriteLine("Interest rate: ");
    while (!decimal.TryParse(Console.ReadLine(), out yearlyInt))
   {
       Console.WriteLine("Interest rate must be numeric.");
   }


    // Step #4:  Prompt the user to enter the monthly deposit
      Console.WriteLine("Deposit (monthly): ");
    while (!decimal.TryParse(Console.ReadLine(), out monthlyDep))
   {
       Console.WriteLine("Monthly deposit must be numeric.");
   }


    future = FutureValue(presentValue, numYears, yearlyInt, monthlyDep);


    Console.WriteLine("********FutureValue********");
    Console.WriteLine("{0:C}", future);
  }

  public static decimal FutureValue(decimal presentValue, int numYears, decimal yearlyInt, decimal monthlyDep)
{
    int months=numYears * 12;
    double interest=(double)yearlyInt/12/100;

    return presentValue * (decimal)Math.Pow((1 + interest), months) + (monthlyDep * (decimal)(Math.Pow((1 + interest), months) -1)/(decimal)interest);
}

  }//end of main method
}//end of class
