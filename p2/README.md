
# LIS4369 - Extensible Enterprise Solutions

## Jarod Barclay

### Project 2 Requirements:


1. LINQ uses programming language syntax to query data.
2. LINQ uses SQL-like syntax to produce usable objects.
3. LINQ can be used to query many different types of data, including relational, XML, and objects. 
4. https://www.microsoft.com/net/tutorials/csharp/getting-started/linq
#### README.md file should include the following items:

* Screenshot of Program Running

> This is a blockquote.
> 

#### Assignment Screenshots:

*Screenshot of Project 2*:

![Project 2 Screenshot](img/ss1.png)

![Project 2 Screenshot](img/ss2.png)

![Project 2 Screenshot](img/ss3.png)

