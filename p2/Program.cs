﻿using System;
using System.Collections.Generic;
using System.Linq;

public class Program
{
    public static void Main()
    {
string requirements =
        @"/////////////////////////////////////////////
        Program Requirements
        Project 2
        Author: Jarod Barclay

        *After* completing the required tutorial, create the following program:

        1) Prompt user for last name. Return Full name, Occupation and Age.

        2) Prop user for age and occupation (Dev or Manager). Return Full name.
        (Must include data validation on numeric data.)

        3. Allow user to press and key to return back to command line

        *Notes:*
        -LINQ uses programming language syntax to query data.
        -Uses SQL-Like syntax to produce usable objects.
        -Can be used to query many different types of data, including relational, XML, and even objects.
        //////////////////////////////////////////////";

        Console.WriteLine(requirements);

        //Find Items in Collections:
        Console.WriteLine("\n***Finding Items in Collections***");
        var people = GenerateListOfPeople();
        var emptyList = new List<Person>();

        //Where:
        Console.WriteLine("\nWhere:");
        var peopleOverTheAgeOf30 = people.Where(x => x.Age > 30);
        foreach(var person in peopleOverTheAgeOf30)
        {
            Console.WriteLine(person.FirstName);
        }

        //Skip:
        Console.WriteLine("\nSkip:");
        //Will ignore Eric and Steve in the list of people
        IEnumerable<Person> afterTwo = people.Skip(2);
        foreach(var person in afterTwo)
        {
            Console.WriteLine(person.FirstName);
        }

        //Take:
        Console.WriteLine("\nTake:");
        //Will only return Eric and Steve from the list of people
        IEnumerable<Person> takeTwo = people.Take(2);
        foreach(var person in takeTwo)
        {
            Console.WriteLine(person.FirstName);
        }

        //Changing Each Item in Collections:
        Console.WriteLine("\n***Changing Each Item in Collections***");
        Console.WriteLine("\nSelect:");
        IEnumerable<string> allFirstNames = people.Select(x => x.FirstName);
        foreach(var firstName in allFirstNames)
        {
            Console.WriteLine(firstName);
        }

        //FullName:
        Console.WriteLine("\nFullname class and objects:");
        IEnumerable<FullName> allFullNames = people.Select(x => new FullName { First = x.FirstName, Last = x.LastName });
        foreach(var fullName in allFullNames)
        {
            Console.WriteLine($"{fullName.Last}, {fullName.First}");
        }

        //First Or Default:
        Console.WriteLine("\n***Find One Item in Collections***");
        Console.WriteLine("\nFirstOrDefault:");
        Person firstOrDefault = people.FirstOrDefault();
        Console.WriteLine(firstOrDefault.FirstName);

        //First Or Default as Filter:
        Console.WriteLine("\nFirstOrDefault as filter:");
        var firstThirtyYearOld1 = people.FirstOrDefault(x => x.Age == 30);
        var firstThirtyYearOld2 = people.Where(x => x.Age == 30).FirstOrDefault();
        Console.WriteLine(firstThirtyYearOld1.FirstName); //Will output "Brendan"
        Console.WriteLine(firstThirtyYearOld2.FirstName); //Will also output "Brendan"

        //How or Dafault works:
        Console.WriteLine("\nHow OrDefault works:");

        List<Person> emptyList1 = new List<Person>();
        Person willBeNull = emptyList.FirstOrDefault();

        List<Person> people1 = GenerateListOfPeople();
        Person willAlsoBeNull = people.FirstOrDefault(x => x.FirstName == "John");

        Console.WriteLine(willBeNull == null); // true
        Console.WriteLine(willAlsoBeNull == null); //true

        //Last Or Default as Filter
        Console.WriteLine("\nLastOrDefault as filter:");
        Person lastOrDefault = people.LastOrDefault();
        Console.WriteLine(lastOrDefault.FirstName);
        Person lastThirtyYearOld = people.LastOrDefault(x => x.Age == 30);
        Console.WriteLine(lastThirtyYearOld.FirstName);

        //Single Or Default as filter
        Console.WriteLine("\nSingleOrDefault as filter:");

        Person single = people.SingleOrDefault(x => x.FirstName == "Eric");
        Console.WriteLine(single.FirstName);
        // Uncomment the next line to see it throw an exception
        // Person singleDev = people.SingleOrDefault(x => x.Occupation == "IT Help Desk");

        //Finding Data About Collections
        Console.WriteLine("\n***Finding Data About Collections***");

        //Count:
        Console.WriteLine("\nCount():");
        int numberOfPeopleInList = people.Count();
        Console.WriteLine(numberOfPeopleInList);

        Console.WriteLine("\nCount() with predicate expression:");
        int peopleOverTwentyFive = people.Count(x => x.Age > 25);
        Console.WriteLine(peopleOverTwentyFive);

        //Any:
        Console.WriteLine("\nAny():");
        bool thereArePeople = people.Any();
        Console.WriteLine(thereArePeople);
        bool thereAreNoPeople = emptyList.Any();
        Console.WriteLine(thereAreNoPeople);

        //ALL:
        Console.WriteLine("\nAll:");
        bool allDevs = people.All(x => x.Occupation == "Dev");
        Console.WriteLine(allDevs);
        bool everyoneAtLeastTwentyFour = people.All(x => x.Age >= 24);
        Console.WriteLine(everyoneAtLeastTwentyFour);

        //Converting Results to Collections:
        Console.WriteLine("\n***Converting Results to Collections***");

        //To List():
        Console.WriteLine("\nToList():");
        List<Person> listOfDevs = people.Where(x => x.Occupation == "Dev").ToList(); //This will return a List<Person>
           foreach(var person in listOfDevs)
        {
            Console.WriteLine(person.FirstName);
        }

        //To Array():
        Console.WriteLine("\nToArray():");
        Person[] arrayOfDevs = people.Where(x => x.Occupation == "Dev").ToArray(); //This will return a Person[] array
              foreach(var person in arrayOfDevs)
        {
            Console.WriteLine(person.FirstName);
        }

        //Required Program:
        Console.WriteLine("\n***Required Program:***");
        Console.WriteLine("\nNote: Searches are case sensitive.");

        //Last Name
        Console.WriteLine("\nPlease enter last name:");
        string lName = Console.ReadLine();

        var fullInformation = people.Where(x => x.LastName == lName);
          foreach(var person in fullInformation)
          {
                Console.WriteLine("Matching criteria: " + person.FirstName + " " + person.LastName + " is a " + person.Occupation + ", and is " + person.Age + " years old.");
          }

        int age = 0;
        Console.Write("\nPlease enter age: ");
            while(!int.TryParse(Console.ReadLine(), out age))
                {
                    Console.WriteLine("Age must be numeric: ");
                }

        Console.Write("\nPlease enter Occupation (Dev or Manager): ");
        string occupation = Console.ReadLine();

        //var userName = people.Where(x => (x.Age == age) && (x.Occupation == occupation));
         var userName = people.Where(x => x.Age == age)
        .Where(y => y.Occupation == occupation);

        foreach(var person in userName)
            {
                Console.WriteLine("Matching criteria: " + person.FirstName + " " + person.LastName);
            }

        Console.WriteLine();
        Console.Write("\nPress any key to exit!");
        Console.ReadKey();

    }

    public static List<Person> GenerateListOfPeople()
    {
        var people = new List<Person>();

        people.Add(new Person { FirstName = "Eric", LastName = "Fleming", Occupation = "Dev", Age = 24 });
        people.Add(new Person { FirstName = "Steve", LastName = "Smith", Occupation = "Manager", Age = 40 });
        people.Add(new Person { FirstName = "Brendan", LastName = "Enrick", Occupation = "Dev", Age = 30 });
        people.Add(new Person { FirstName = "Jane", LastName = "Doe", Occupation = "Dev", Age = 35 });
        people.Add(new Person { FirstName = "Samantha", LastName = "Jones", Occupation = "Dev", Age = 24 });


        return people;
    }
}

public class Person
{
    public string FirstName { get; set; }
    public string LastName { get; set; }
    public string Occupation { get; set; }
    public int Age { get; set; }
}

public class FullName
{
    public string First { get; set; }
    public string Last { get; set; }
}
