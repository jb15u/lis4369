﻿using System;

namespace a5
{
    public class Program
    {
        public static void Main(string[] args)
        {
string requirements =
@"//////////////////////////
Program Requirements:
Using Classes - Inheritance
  Also, Overloading (Compile-Time Polymorphism) and Overriding (Run-time Polymorphism)
Author: Jarod Barclay

Vehicle and Car class definitions must be stored in seperate files apart from Main() method

1) Create Vehicle class (base/super/parent class)

  A) Create two private data members(fields/instance variables):
    1. milesTraveled (float)
    2. gallonsUsed (float)

  B) Create four properties:
    1. Manufacturer (string)
    2. Make (string)
    3. Model (string)
    4. MPG (float) (read-only)

  C) Create two setter/mutator methods:
    1. SetMiles
    2. SetGallons

  D) Create four getter/accessor methods:
    1. GetMiles
    2. GetGallons
    3. GetObjectInfo()
    4. GetObjectInfo(arg)(overloaded method): accepts string to be used as a separator for display purposes

  E) Create two constructors:
    1. default constructor (accepts no arguments)
    2. parameterized constructor w/ default parameter values that accepts three arguments

2) Create Car class (derived/sub/child class)

  A) Create one private data members:
    1. style (string)

  B) Create one property:
    1. Style (string) (not auto implemented)

  C) Create two constructors:
    1. default constructor (accepts no agruments)
    2. parameterized constructor w/ default parameters that accepts four arguments

  D) Create one getter/accessor method:
    1. GetObjectInfo(arg)(overridden method): accepts string arg to be used as a separator for display purposes

3) Instantiate three vehicle objects:
  a. one from default constructor, display data member values
  b. two from parameterized constructor (passing arguments to its constructors parameters), display data member values

4) Instantiate one car object:
  a. one from parameterized constructor (passing arguments to its constructors parameters), display data member values

5) Must include data validation on numeric input

6) Allow user to press any key to return back to command line.
//////////////////////////";
  Console.WriteLine(requirements);

  Console.WriteLine("\nNow: " + DateTime.Now.ToString("ddd, M/d/yy h:mm:ss t"));

          Console.WriteLine();

          Console.WriteLine("\nvehicle1 - (instantiating new object from default constructor)");
          Vehicle vehicle1 = new Vehicle();

          Console.WriteLine(vehicle1.GetObjectInfo());

          // call parameterized constructor
          Console.WriteLine("\nvehicle2 (user input): Call parameterized base constructor (accepts arguments):");

          //initialize arguments
          string p_manufacturer = "";
          string p_make = "";
          string p_model = "";
          float p_miles = 0.0f;
          float p_gallons = 0.0f;
          string p_separator = "";

          // get user input
          Console.Write("Manufacturer (alpha): ");
          p_manufacturer = Console.ReadLine();

          Console.Write("Make (alpha): ");
          p_make = Console.ReadLine();

          Console.Write("Model (alpha): ");
          p_model = Console.ReadLine();

          Console.Write("Miles driven (float): ");
          while (!float.TryParse(Console.ReadLine(), out p_miles))
          {
            Console.Write("Miles must be numeric: ");
          }

          Console.Write("Gallons used (float): ");
          while (!float.TryParse(Console.ReadLine(), out p_gallons))
          {
            Console.Write("Gallons must be numeric: ");
          }

          Console.WriteLine("\nvehicle2 - (instantiating new object, passing *only 1st arg*)\n" +
            "(Demos why use constructor with default parameter values):");
          Console.WriteLine("*Note*:only 1st arg passed to constructor, others are default values.");
          Vehicle vehicle2 = new Vehicle(p_manufacturer);

          Console.WriteLine(vehicle2.GetObjectInfo());

          Console.WriteLine("\nUser input: vehicle2 - passing arg to overloaded GetObjectInfo(arg):");
          Console.Write("Delimiter (, : ;): ");
          p_separator = Console.ReadLine();
          Console.WriteLine(vehicle2.GetObjectInfo(p_separator)); // use overloaded method

          Console.WriteLine("\nvehicle3 - (instantiating new object, passing *all* vehicle args):");
          Vehicle vehicle3 = new Vehicle(p_manufacturer, p_make, p_model);

          vehicle3.SetGallons(p_gallons);
          vehicle3.SetMiles(p_miles);

          Console.WriteLine("\nUser input: vehicle3 - passing arg to overloaded GetObjectInfo(arg)");
          Console.Write("Delimiter (, : ;): ");
          p_separator = Console.ReadLine();
          Console.WriteLine(vehicle3.GetObjectInfo(p_separator));

          Console.WriteLine("\nDemonstrating Polymorphism (new derived object):");
          Console.WriteLine("(User input: car1 - calling parameterized base class constructor explictly.)\n");

          // initialize arguments
          string p_style = "";

          // get user input
          Console.Write("Manufacturer (alpha): ");
          p_manufacturer = Console.ReadLine();

          Console.Write("Make (alpha): ");
          p_make = Console.ReadLine();

          Console.Write("Model (alpha): ");
          p_model = Console.ReadLine();

          Console.Write("Miles driven (float): ");
          while (!float.TryParse(Console.ReadLine(), out p_miles))
          {
            Console.Write("Miles must be numeric: ");
          }

          Console.Write("Gallons used (float): ");
          while (!float.TryParse(Console.ReadLine(), out p_gallons))
          {
            Console.Write("Gallons must be numeric: ");
          }

          // 2dr, 4dr, Convertible, Coupe, Hatchback
          Console.Write("Style (alphanumeric): ");
          p_style = Console.ReadLine();

          Console.WriteLine("\ncar1 - (instantiating new derived object, passing *all* args):");
          Car car1 = new Car(p_manufacturer, p_make, p_model, p_style);

          // car class has access to Vehicle's SetGallons and SetMiles methods
          car1.SetGallons(p_gallons);
          car1.SetMiles(p_miles);

          Console.WriteLine("\nUser input: car1 - passing arg to overridden (diff scope - inheritance) GetObjectInfo(arg):");
          Console.Write("Delimiter (, : ;): ");
          p_separator = Console.ReadLine();
          Console.WriteLine(car1.GetObjectInfo(p_separator));

          Console.WriteLine();
          Console.WriteLine("\nPress any key to exit!");
          Console.ReadKey();

        }
    }
}
