
# LIS4369 - Extensible Enterprise Solutions

## Jarod Barclay

### Assignment 5 Requirements:


1. Display assignment requirements and my name as the author
2. Display current date/time
3. Create two classes: vehicle and car
4. Overloading (Compile-Time Polymorphism) and Overriding (Run-time Polymorphism)
5. Must include data validation on numeric data

#### README.md file should include the following items:

* Screenshot of Program

> This is a blockquote.
> 

#### Assignment Screenshots:

*Screenshot of Program running*:

![Inheritance Screenshot](img/ss1.png)
![Inheritance Screenshot](img/ss2.png)

