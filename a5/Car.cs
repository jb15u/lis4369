﻿using System;

namespace a5
{
    public class Car : Vehicle
    {
        private string style;

        public string Style
        {
          get { return style; }
          set { style = value; }
        }

        // default constructor for derived class
        public Car()
        {
          this.Style = "Default style";

          Console.WriteLine("\nCreating derived object from default constructor (accepts no arguments):");
        }

        // call base class explicitly: must bring in base class parameter names, and add any new data members
        public Car(string mn = "Manufacturer", string mk = "Make", string md = "Model", string st = "Style") : base(mn, mk, md)
        {
          this.Style = st;
          Console.WriteLine("\nCreating derived object from parameterized constructor (accepts arguments):");
        }

        // overridden method: same name, same signature, differect scope (inheritance)
        public override string GetObjectInfo(string sep)
        {
          //calls overloaded method from base class
          return base.GetObjectInfo(sep) + sep + Style;
        }
    }
}
