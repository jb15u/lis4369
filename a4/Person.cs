﻿using System;

namespace a4
{
    public class Person
    {
        protected string fname;
        protected string lname;
        protected int age;

        // default constructor
        public Person()
        {
            fname = "First name";
            lname = "Last name";
            age = 0;

            Console.WriteLine("\nCreating base person object from default constructor (accepts no agruments):");
            Console.WriteLine("\nCreating " + this.fname + " " + this.lname + " person object from default constructor (accepts no arguments):");
        }

        // parameterized constructor that accepts two agruments
        public Person(string fn="", string ln="", int a=0)
        {
          fname = fn;
          lname = ln;
          age = a;

          Console.WriteLine("\nCreating base person object from parameterized constructor (accepts two agruments):");
        }

        // * mutator methods *
        //setter method
        public void SetFname(string fn="")
        {
          fname = fn;
        }

        public void SetLname(string ln="")
        {
          lname = ln;
        }
        public void SetAge(int a=0)
        {
          age = a;
        }

        // * accessor methods *
        // getter method
        public string GetFname()
        {
          return fname;
        }

        public string GetLname()
        {
          return lname;
        }

        public int GetAge()
        {
          return age;
        }

        // When base class declares virtual method, derived class can override method
        public virtual string GetObjectInfo()
        {
          return fname + " " + lname + " is " + age.ToString();
        }
    }
}
