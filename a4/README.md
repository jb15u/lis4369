
# LIS4369 - Extensible Enterprise Solutions

## Jarod Barclay

### Assignment 4 Requirements:


1. Backward-engineer (using .NET Core) console application screenshots
2. Display assignment requirements and my name as the author
3. Display current date/time
4. Create two classes: person and student
5. Must include data validation on numeric data

#### README.md file should include the following items:

* Screenshot of Inheritance

> This is a blockquote.
> 

#### Assignment Screenshots:

*Screenshot of Inheritance*:

![Inheritance Screenshot](img/ss1.png)
![Inheritance Screenshot](img/ss2.png)
![Inheritance Screenshot](img/ss3.png)

