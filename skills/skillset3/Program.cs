using System;

namespace ConsoleApplication
{
    public class Program
    {
        public static void Main(string[] args)
        {
        	// initialize variables
        	string requirements = @"Program Requirements:
	1) Use intrinsic method to display date/time;
	2) Use two types of selection structures: if...else if...else, and switch;
	3) Initialize suitable variable(s): use decimal data type for bill;
	4) Prompt and capture user input;
	5) Display money in currency format;
	6) Allow user to press any key to return back to command line.";
			decimal bill = 100;
			double books;
			double cost = 0.0;
			double bookPrice = 50.00;
			int color = 0;
			double totalCost = 0.0;

		// print requirements
		Console.WriteLine(requirements);

		Console.WriteLine("Now: " + DateTime.Now.DayOfWeek + ", " + DateTime.Now);
		Console.WriteLine("");

		// if...else case
		Console.WriteLine("****if...else if...else example****");
		Console.WriteLine("Your bill is $" + bill);
		Console.WriteLine("");
		// book discounts
		Console.WriteLine("Discounts:");
		Console.WriteLine("6+: 30% off");
		Console.WriteLine("3-5: 20% off");
		Console.WriteLine("2: 10% off");
		Console.WriteLine("1: Regular price");
		Console.WriteLine("");

		Console.WriteLine("How many books did you buy? ");
		books = int.Parse(Console.ReadLine());

		if (books == 1)
		{
			cost = books * bookPrice;

			Console.WriteLine("\nYour cost: $" + cost + ".");
		}

		else if (books == 2)
		{
			cost = (books * bookPrice);

			totalCost = (cost - (cost*(00.10)));

			Console.WriteLine("\nYour cost: $" + totalCost + ".");
		}

		else if (books == 3 || books == 4 || books == 5)
		{
			cost = (books * bookPrice);

			totalCost = (cost - (cost*(00.20)));

			Console.WriteLine("\nYour cost: $" + totalCost + ".");
		}

		else if (books >= 6)
		{
			cost = (books * bookPrice);

			totalCost = (cost - (cost*(00.30)));

			Console.WriteLine("\nYour cost: $" + totalCost + ".");
		}

		else
		{
			Console.WriteLine("Please enter a valid number next time.");
		}

		Console.WriteLine("");
		Console.WriteLine("****switch example****");
		Console.WriteLine("");
		Console.WriteLine("1 - red");
		Console.WriteLine("2 - green");
		Console.WriteLine("3 - blue");
		Console.WriteLine("");

		Console.WriteLine("What is your favorite color? ");
		color = int.Parse(Console.ReadLine());

		switch (color)
		{
			case 1:
				Console.WriteLine("Your favorite color is red.");
				break;
			case 2:
				Console.WriteLine("Your favorite color is green.");
				break;
			case 3:
				Console.WriteLine("Your favorite color is blue.");
				break;
			default:
				Console.WriteLine("Not a valid color option.");
				break;
		}


		// exit
		Console.WriteLine("\nPress any key to exit!");
		Console.ReadKey();




        }
    }
}
