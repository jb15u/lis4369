using System;

namespace skillset11
{
	public class Program
	{

      static void Main(string[] args)
            {

  			// Name and Date
            Console.WriteLine("Author: Jarod Barclay");
            Console.WriteLine();
            DateTime localDate = DateTime.Now;
            Console.WriteLine("Now: " + localDate.ToString());
            Console.WriteLine();


            //Varible
            double num1;



            // First User Entery
            Console.WriteLine("Please enter a number: ");
            Console.WriteLine("num1: ");
            while(!double.TryParse(Console.ReadLine(), out num1))
            {

            	Console.WriteLine("num1 must be numeric");

            }//closing of while
            Console.WriteLine("");


            // Pass by Vaule - is passing a value
            Console.WriteLine("Call PassByVal()");
            PassByVal(num1);
            Console.WriteLine("In Main() num1 = " + num1);
            Console.WriteLine("");

            // Pass by reference
            Console.WriteLine("Call PassByRef()");
            PassByRef(ref num1);
            Console.WriteLine("In Main() num1 = " + num1);
            Console.WriteLine("");

            Console.WriteLine("Press any key to exit!");
            Console.ReadKey();

    		}// closing of MAIN


    	// Passing by Value
        static void PassByVal(double num1)
            {
                Console.WriteLine("Inside PassByVal() method.");
                num1 += 5;
                Console.WriteLine("Added 5 to number passed by value = " + num1);
            }// closing of PassByVal



        // Passing by reference
        static void PassByRef(ref double num1)
            {
                Console.WriteLine("Inside PassByRef() method.");
                num1 += 5;
                Console.WriteLine("Added 5 to number passed by reference = " + num1);
            }//closing of PassByRed


	}//closing of class
}// closing namespace
