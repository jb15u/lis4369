using System;

namespace ConsoleApplication
{


	public class Program
	{

	public static void Main(string[] args)
	{
		string intro= @"Author: Jarod Barclay
Now: ";

		Console.Write(intro);

		DateTime localdate= DateTime.Now;
		Console.WriteLine(localdate);


		string firstName;
		string lastName;

	 Person person1= new Person();


	 Console.Write("First Name: " + person1.GetFname() + "\n");

	 Console.Write("Last Name: " + person1.GetLname() + "\n");

	 Console.WriteLine("\nModify default constructor object's data member values");
	 Console.WriteLine("Using setter getter methods");


	 	Console.Write("First name: ");
	 	 	 firstName= Console.ReadLine();

	 	 	 	 Console.Write("Last Name: ");
				lastName= Console.ReadLine();

			person1.SetFname(firstName);
			person1.SetLname(lastName);

			Console.WriteLine("\nDisplay object's new data member values: ");


	 	 	 Console.Write("First Name: " + person1.GetFname() + "\n");
	 	 	 	Console.Write("Last Name: " + person1.GetLname() + "\n");


			Console.WriteLine("\nCall Parameterized constructor (accepts two arguments): ");

			Console.Write("First name: ");
	 	 	 firstName= Console.ReadLine();

	 	 	 	 Console.Write("Last Name: ");
				lastName= Console.ReadLine();

			Person person2= new Person(firstName, lastName);

			Console.Write("First name: " + person2.GetFname() + "\n");

			Console.Write("Last name: " + person2.GetLname() + "\n");

			Console.WriteLine("\nCreate Person object from parameterized constructor(accepts two arguments): ");

			Console.WriteLine("Full name: "+ person2.GetFname() + " " + person2.GetLname() );

}
}
}
