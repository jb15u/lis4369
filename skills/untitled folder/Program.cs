using System;

namespace skillset11
{
	public class Program
	{

    static void Main(string[] args)
            {


            Console.WriteLine("Author: Jarod Barclay");
            Console.WriteLine();
            DateTime localDate = DateTime.Now;
            Console.WriteLine("Now: " + localDate.ToString());
            Console.WriteLine();

            double num1;


            Console.WriteLine("Please enter a number: ");
            Console.WriteLine("num1: ");
            while(!double.TryParse(Console.ReadLine(), out num1))
            {

            	Console.WriteLine("num1 must be numeric");

            }
            Console.WriteLine("");

            // Pass By Value
            Console.WriteLine("Call PassByVal()");
            PassByVal(num1);
            Console.WriteLine("In Main() num1 = " + num1);
            Console.WriteLine("");

            // Pass By Reference
            Console.WriteLine("Call PassByRef()");
            PassByRef(ref num1);
            Console.WriteLine("In Main() num1 = " + num1);
            Console.WriteLine("");

            Console.WriteLine("Press any key to exit!");
            Console.ReadKey();

    		}


    	// Passing by Value
        static void PassByVal(double num1)
            {
                Console.WriteLine("Inside PassByVal() method.");
                num1 += 5;
                Console.WriteLine("Added 5 to number passed by value = " + num1);
            }

        // Passing by reference
        static void PassByRef(ref double num1)
            {
                Console.WriteLine("Inside PassByRef() method.");
                num1 += 5;
                Console.WriteLine("Added 5 to number passed by reference = " + num1);
            }
	}
}
