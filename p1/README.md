
# LIS4369 - Extensible Enterprise Solutions

## Jarod Barclay

### Project 1 Requirements:


1. Backward-engineer (using .NET Core) console application screenshots
2. Display assignment requirements and my name as the author
3. Display current date/time
4. Perform and display room size calculations, include data validation, & round to two decimal places
5. Each data member must have get/set methods, also GetArea and GetVolume

#### README.md file should include the following items:

* Screenshot of Room Calculator

> This is a blockquote.
> 

#### Assignment Screenshots:

*Screenshot of room calculator*:

![Room Calculator Screenshot](img/p1.png)

