﻿using System;

namespace P1
{
    public class Program
    {
	public static void Main(string[] args)
	{
	string requirements =
	@"///////////////////////
	Program Requirements:
	P1 - Room Size Calculator Using Classes
	Author: Jarod Barclay
	1) Create Room class;
	2) Create following fields (aka properties or data members):
	   a. private string type; // room type
	   b. private double length; // room length
	   c. private double width; // room width
	   d. private double height; // room height
	3) Create two constructors:
	   a. Default constructor
	   b. Parameterized constructor that accepts four arguments
	4) Create the following mutator (aka setter) methods:
	   a. SetType
	   b. SetLength
	   c. SetWidth
	   d. SetHeight
	5) Create the following accessor (aka getter) methods:
	   a. GetType
	   b. GetLength
	   c. GetWidth
	   d. GetHeight
	   e. GetArea
	   f. GetVolume
	6) Must include the following functionality:
	   a. Display room size calculations in feet 
	   b. Must include data validation
	   c. Round to two decimal places
	7) Allow user to press any key to return back to command line.
	///////////////////////";
	Console.WriteLine(requirements);

	Console.WriteLine("\nNow: " + DateTime.Now.ToString("ddd, M/d/yy h:mm:ss t"));
 	Console.WriteLine();
  
        Room room1 = new Room();

	// use getter/accessor methods    
	Console.Write("Room Type: ");
	Console.WriteLine(room1.GetType());

	Console.Write("Room Length: ");
        Console.WriteLine(room1.GetLength());

	Console.Write("Room Width: ");
        Console.WriteLine(room1.GetWidth());

	Console.Write("Room Height: ");
        Console.WriteLine(room1.GetHeight());

	Console.Write("Room Area: ");
        Console.WriteLine(room1.GetArea().ToString("F2") + " sq ft");

	Console.Write("Room Volume: ");
        Console.WriteLine(room1.GetVolume().ToString("F2") + " cu ft");
        
	Console.Write("Room Volume: ");



	Console.WriteLine((room1.GetVolume() / 27).ToString("F2") + " cu yd");

	Console.WriteLine("\nModify " + room1.GetType() + " room objects data member values:");
	Console.WriteLine("Use setter/getter methods:");

	// initialize variables
	string rtype = "";
	double rlength = 0.0;
	double rwidth = 0.0;
	double rheight = 0.0;

	Console.Write("Room type: ");
	rtype = Console.ReadLine();

	Console.Write("Room length: ");
	while (!double.TryParse(Console.ReadLine(), out rlength))
	{
	    Console.WriteLine("Room length must be numeric.");
	}

	Console.Write("Room width: ");
	while (!double.TryParse(Console.ReadLine(), out rwidth))
	{
            Console.WriteLine("Room width must be numeric.");
        }	

	Console.Write("Room height: ");
        while (!double.TryParse(Console.ReadLine(), out rheight))
	{
            Console.WriteLine("Room height must be numeric.");
        }

	Console.WriteLine("\nDisplay " + room1.GetType() + " room objects new data member values:");

	// use setter/mutator methods
	room1.SetType(rtype);
	room1.SetLength(rlength);
	room1.SetWidth(rwidth);
	room1.SetHeight(rheight);

	Console.Write("Room Type: ");
	Console.WriteLine(room1.GetType());

	Console.Write("Room Length: ");
        Console.WriteLine(room1.GetLength());

	Console.Write("Room Width: ");
        Console.WriteLine(room1.GetWidth());

	Console.Write("Room Height: ");
        Console.WriteLine(room1.GetHeight());

	Console.Write("Room Area: ");
        Console.WriteLine(room1.GetArea().ToString("F2") + " sq ft");

	Console.Write("Room Volume: ");
        Console.WriteLine(room1.GetVolume().ToString("F2") + " cu ft");

	Console.Write("Room Volume: ");
        Console.WriteLine((room1.GetVolume() / 27).ToString("F2") + " cu yd");

	Console.WriteLine();

	// call parameterized constructor with four parameters
	Console.WriteLine("Call parameterized constructor (accepts four arguments):");

	Console.Write("Room type: ");
	rtype = Console.ReadLine();

	Console.Write("Room length: ");
	while (!double.TryParse(Console.ReadLine(), out rlength))
	{
	    Console.WriteLine("Room length must be numeric.");
	}

	Console.Write("Room width: ");
        while (!double.TryParse(Console.ReadLine(), out rwidth))
        {
            Console.WriteLine("Room width must be numeric.");
        }
	Console.Write("Room height: ");
        while (!double.TryParse(Console.ReadLine(), out rheight))
        {
            Console.WriteLine("Room height must be numeric.");
        }

	
	Console.WriteLine();

	Room room2 = new Room(rtype, rlength, rwidth, rheight);

	Console.Write("Room Type: ");
	Console.WriteLine(room2.GetType());

	Console.Write("Room Length: ");
        Console.WriteLine(room2.GetLength());

	Console.Write("Room Width: ");
        Console.WriteLine(room2.GetWidth());

	Console.Write("Room Height: ");
        Console.WriteLine(room2.GetHeight());

	Console.Write("Room Area: ");
        Console.WriteLine(room2.GetArea().ToString("F2") + " sq ft");

	Console.Write("Room Volume: ");
        Console.WriteLine(room2.GetVolume().ToString("F2") + " cu ft");

	Console.Write("Room Volume: ");
	Console.WriteLine((room2.GetVolume() / 27).ToString("F2") + " cu yd");

	Console.WriteLine("\nPress any key to exit!");
	Console.ReadKey();
    }
 }
}
