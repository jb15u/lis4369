#LIS4369 - Extensible Enterprise Solutions

##Jarod Barclay

*Course Work Links:*

1. [A1 README.md](a1/README.md "My A1 README.md file")
    - Install .NET Core
    - Create hwapp application
    - Create aspnetcoreapp application
    - Provide screenshots of installations
    - Create Bitbucket repo
    - Complete Bitbucket tutorials (bitbucketstationlocations and myteamquotes)
    - Provide git command descriptions
    
2. [A2 README.md](a2/README.md "My A2 README.md file")
    - Backward Engineer Simple Calculator Screenshots (using .NET Core)
    - Intro to ASP.NET Questions

3. [A3 README.md](a3/README.md "My A3 README.md file")
    - Backward Engineer Future Value Calculator Screenshots (using .NET Core)
    - Use decimal data types and currency formatting
    
4. [A4 README.md](a4/README.md "My A4 README.md file")
    - Backward Engineer Inheritance Screenshots (using .NET Core)
    - Person (Base Class) and Student (Derived Class)
    
5. [A5 README.md](a5/README.md "My A5 README.md file")
    - Overloading and Overriding
    - Vehicle (Base Class) and Car (Derived Class)
    
6. [P1 README.md](p1/README.md "My P1 README.md file")
    -  Backward Engineer Room Calculations Screenshots (using .NET Core)
    - Getter/Setter Methods
    
7. [P2 README.md](p2/README.md "My P2 README.md file")
    - Introducing LINQ (Language Integrated Query)
    - Complete LINQ Tutorial