
# LIS4369 - Extensible Enterprise Solutions

## Jarod Barclay

### Assignment 1 Requirements:

*Three Parts:*

1. Distributed Version Control with Git and Bitbucket
2. Development Installations
3. Questions

#### README.md file should include the following items:

* Screenshot of hwapp application running
* Screenshot of aspnetcoreapp application running
* git commands w/ short descriptions
* Bitbuckcet repo links: this assignment and the completed tutorial (bitbucketstationlocations and myteamquotes)

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>
> #### Git commands w/short descriptions:

1. git init- initialize .git inside current working directory
2. git status- checks status of git repository
3. git add- adds a file to git repository
4. git commit- saves changes to repository
5. git push- push commits from local branch to remote repository
6. git pull- pulls from repository to local branch
7. git log- shows the git commit history for a project


#### Assignment Screenshots:

*Screenshot of hwapp running http://localhost*:

![HWAPP Installation Screenshot](img/ss2.png)

*Screenshot of aspnetcoreapp running*:

![aspnetcoreapp Installation Screenshot](img/ss1.png)


#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/jb15u/bitbucketstationlocations/ "Bitbucket Station Locations")

*Tutorial: Request to update a teammate's repository:*
[A1 My Team Quotes Tutorial Link](https://bitbucket.org/jb15u/myteamquotes/ "My Team Quotes Tutorial")
