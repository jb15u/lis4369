
# LIS4369 - Extensible Enterprise Solutions

## Jarod Barclay

### Assignment 2 Requirements:

*Three Parts:*

1. Backward-engineer (using .NET Core) console application screenshots
2. Display assignment requirements and my name as the author
3. Display current date/time
4. Perform and display 4 mathematical operations

#### README.md file should include the following items:

* Screenshot of valid operation
* Screenshot of invalid operation

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>

#### Assignment Screenshots:

*Screenshot of valid operation*:

![Valid Operation Screenshot](img/ss1.png)

*Screenshot of invalid operation*:

![Invalid Operation Screenshot](img/ss2.png)


#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/jb15u/bitbucketstationlocations/ "Bitbucket Station Locations")

*Tutorial: Request to update a teammate's repository:*
[A1 My Team Quotes Tutorial Link](https://bitbucket.org/jb15u/myteamquotes/ "My Team Quotes Tutorial")
