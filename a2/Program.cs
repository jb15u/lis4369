using System;

namespace ConsoleApplication
{
    public class Program
    {
        public static void Main(string[] args)
        {
string requirements =
@"//////////////////////////
Program Requirements:
Methods: Pass By Value vs Pass By Reference
Author: Jarod Barclay

1) Create two methods:
  a. PassByVal
  b. PassByRef

2) Initialize one (double) variable: num1

3) Allow user input and data validate

4) Pass num1 to each method above

5) Allow user to press any key to return back to command line.
//////////////////////////";

Console.WriteLine(requirments);

Console.WriteLine("\nNow: " + DateTime.Now.ToString("ddd, M/d/yy h:mm:ss t"));

        Console.WriteLine();

        double num1 = 0.0;

        Console.WriteLine("Please enter a number: ");
        while (!double.TryParse(Console.ReadLine(), out num1))
      	{
      	    Console.WriteLine("num1 must be numeric.");
      	}

        public static void PassByVal(double val)
        {
          Console.WriteLine("Call PassByVal().");
          Console.WriteLine("Inside PassByVal() method.");

          val = num1 + 5;
          Console.WriteLine("Added 5 to number passed by value = " + val);
          Console.WriteLine("In Main() num1 = " + num1);
      	}

        public static void PassByRef(ref double val2)
        {
          Console.WriteLine("Call PassByRef().");
          Console.WriteLine("Inside PassByRed() method.");
          val2 = num1 + 5;
          Console.WriteLine("Added 5 to number passed by reference = " + val2);
          Console.WriteLine("In Main() num1 = " + num1);
          Console.WriteLine();
        }

          Console.WriteLine("\nPress any key to exit!");
          Console.ReadKey();
        }

    }
}
